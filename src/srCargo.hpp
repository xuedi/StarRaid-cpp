/**
 * @addtogroup shared
 * @{
 *
 * @file   srCargo.hpp
 * @author xuedi (xuedi@beijingcode.org)
 * @date   2015-05-29
 * @brief  Contains the cargo of the object
 *
 * License:
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright:
 *    2005-2017 by Daniel (xuedi) Koch
 *
 */

#ifndef SRCARGO_H
#define SRCARGO_H


#include <string.h>
#include <iostream>
#include <sstream>


using namespace std;

class srCargo {


    public:
        srCargo();
        virtual ~srCargo();

        string type;
        int amount;

    protected:


    private:


};

#endif // SRCARGO_H
/** @} group*/